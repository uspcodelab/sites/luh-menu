import { Event, Messages } from '../types';

export const agHackMessages: Messages = {
  'pt-BR': {
    title: 'USP AgHack',
    date: 'Julho de 2019',
    local: 'ESALQ - USP',
    description: `O USP AGHack será um hackathon realizado em parceria com a AUSPIN
    (Agência USP de Inovação) e o campus de Piracicaba (ESALQ). Através da aliança da tecnologia
    com os desafios do campo - comumente chamado de AGTech, será buscado soluções inovadoras
    para os problemas agrícolas atuais.`,
  },
  'en-US': {
    title: 'USP AgHack',
    date: 'July 2019',
    local: 'ESALQ - USP',
    description: `USP AGHack will be hackathon organized with AUSPIN
    (USP Agency of Innovation) and the Piracicaba Campi (ESALQ). Through the allience between
    technology with the challengs of farming - commonly called AGTech, innovative solutions will
    sought out to the current agricultural problems.`,
  },
};

const aghack: Event = {
  id: 'aghack',
  participants: 40,
  color: 'green',
  linearGradient: 'linear-gradient(180deg, #51d88a 0%, #1f9d55 100%)',
  quotas: [
    {
      name: 'Copper',
      prices: {
        'pt-BR': 2500,
        'en-US': 750,
      },
      benefitsList: [
        {
          'pt-BR': `Agradecimento e reconhecimento do patrocinador em todas as
          atividades relacionadas ao evento.`,
          'en-US': `Thanks and recognition to the sponsor in all activities releated
          to the event`,
        },
        {
          'pt-BR': `Possibilidade de distribuir folders, brindes e outros materiais
          relevantes do patrocinador`,
          'en-US': `Possibility to distribute folders, gifts and other
          relevant materials from the sponsor`,
        },
        {
          'pt-BR': `Identificação da marca patrocinadora em todo material de
          divulgação do evento: online e offline`,
          'en-US': `Identification of the sposoring brand in all marketing material
          online and offline`,
        },
      ],
    },
    {
      name: 'Steel',
      prices: {
        'pt-BR': 5000,
        'en-US': 1500,
      },
      benefitsList: [
        {
          'pt-BR': `Todos os benefícios da Copper`,
          'en-US': `All benefits from Copper`,
        },
        {
          'pt-BR': `Oportunidade de trazer visitantes ao evento`,
          'en-US': `Opportunity to bring visitors to the event`,
        },
        {
          'pt-BR': `Oportunidade de exibir banner do patrocinador no evento`,
          'en-US': `Opportunity to bring a sponsor's banner to the event`,
        },
      ],
    },
    {
      name: 'Bronze',
      prices: {
        'pt-BR': 7500,
        'en-US': 2250,
      },
      benefitsList: [
        {
          'pt-BR': `Todos os benefícios da Steel`,
          'en-US': `All benefits from Steel`,
        },
        {
          'pt-BR': `Possibilidade de oferecer prêmios às equipes participantes`,
          'en-US': `Possibility to offer awards to the competing teams`,
        },
        {
          'pt-BR': `Possibilidade de apresentação de até 5 minutos do patrocinador
          na abertura do evento`,
          'en-US': `Possibility to make a up to a 5 minutes presentation about the
          sponsor in the opening of the event`,
        },
      ],
    },
    {
      name: 'Silver',
      prices: {
        'pt-BR': 10000,
        'en-US': 3000,
      },
      benefitsList: [
        {
          'pt-BR': `Todos os benefícios da Bronze`,
          'en-US': `All benefits from Bronze`,
        },
        {
          'pt-BR': `Possibilidade de ter um representante do patrocinador entre
          os mentores do evento`,
          'en-US': `Possibility to have a sponsor's representative between the mentors
          of the event`,
        },
        {
          'pt-BR': `Possibilidade de apresentação de até 15 minutos do patrocinador
          na abertura do evento`,
          'en-US': `Possibility to make a up to 15 minutes presentation about the
          sponsor in the opening of the event`,
        },
      ],
    },
    {
      name: 'Gold',
      prices: {
        'pt-BR': 15000,
        'en-US': 4500,
      },
      benefitsList: [
        {
          'pt-BR': `Todos os benefícios da Silver`,
          'en-US': `All benefits from Silver`,
        },
        {
          'pt-BR': `Possibilidade de acesso aos emails dos participantes do evento
          que consentiram tal acesso`,
          'en-US': `Possibility to access the emails of the participants of the event
          that allowed it`,
        },
        {
          'pt-BR': `Possibilidade de apontar um representante do patrocinador
          como juíz do evento`,
          'en-US': `Possibility to point a sponsor's representative as a judge of
          the event`,
        },
      ],
    },
  ],
};

export default aghack;
