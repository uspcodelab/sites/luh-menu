import { Event, Messages } from '../types';

export const deepHackMessages: Messages = {
  'pt-BR': {
    title: 'DeepHack',
    date: '5 a 19 de Outubro de 2019',
    local: 'empresa parceira',
    description: `DeepHack será uma Hackerfest, na qual os participantes usarão as bases de dados disponibilizadas
    pelo TCE-SP para identificar quais municípios de São Paulo estão cumprindo com as metas do desenvolvimento
    sustentável propostas pela ONU, propondo novas políticas públicas. No dia de abertura,
    teremos palestras sobre a base de dados e os problemas a serem abordados, oficinas sobre ciência de dados e
    aprendizado de máquina, atividades de formação de times. No segundo dia, o encerramento, teremos as
    apresentações das soluções propostas e análise dos dados feitas, sessão de julgamento e premiação das equipes.
    Os participantes terão 2 semanas para trabalhar em suas análises e receber mentoria remota de especialistas de
    empresas parceiras.`,
  },
  'en-US': {
    title: 'DeepHack',
    date: 'October 5 to 19 of 2019',
    local: 'partner company',
    description: `DeepHack will be a Hackerfest, in which the teams will use datasets publisheds
    by TCE-SP to identify which São Paulo cities are achieving sustainable developement goals proposed by UN,
    coming up with new public politics. In the opening day, we will have lectures about the datasets and the problems
    to be solved, data science and machine learning, and activities to form teams. In the closing event, there will be
    the presentations of the analysis and proposed solutions, judging sessions and hadling of the awards. The teams will
    have two weeks between events to work on their analysis and receive remote mentorship from experts from the aiding
    companies.`,
  },
};

const deephack: Event = {
  id: 'deephack',
  participants: 60,
  color: 'blue',
  linearGradient: 'linear-gradient(180deg, #0081ff 0%, #00d1ff 100%)',
  quotas: [
    {
      name: 'Copper',
      prices: {
        'pt-BR': 2500,
        'en-US': 750,
      },
      benefitsList: [
        {
          'pt-BR': `Agradecimento e reconhecimento do patrocinador em todas as
          atividades relacionadas ao evento.`,
          'en-US': `Thanks and recognition to the sponsor in all activities releated
          to the event`,
        },
        {
          'pt-BR': `Possibilidade de distribuir folders, brindes e outros materiais
          relevantes do patrocinador`,
          'en-US': `Possibility to distribute folders, gifts and other
          relevant materials from the sponsor`,
        },
        {
          'pt-BR': `Identificação da marca patrocinadora em todo material de
          divulgação do evento: online e offline`,
          'en-US': `Identification of the sposoring brand in all marketing material
          online and offline`,
        },
      ],
    },
    {
      name: 'Steel',
      prices: {
        'pt-BR': 5000,
        'en-US': 1500,
      },
      benefitsList: [
        {
          'pt-BR': `Todos os benefícios da Copper`,
          'en-US': `All benefits from Copper`,
        },
        {
          'pt-BR': `Oportunidade de trazer visitantes ao evento`,
          'en-US': `Opportunity to bring visitors to the event`,
        },
        {
          'pt-BR': `Oportunidade de exibir banner do patrocinador no evento`,
          'en-US': `Opportunity to bring a sponsor's banner to the event`,
        },
      ],
    },
    {
      name: 'Bronze',
      prices: {
        'pt-BR': 10000,
        'en-US': 3000,
      },
      benefitsList: [
        {
          'pt-BR': `Todos os benefícios da Steel`,
          'en-US': `All benefits from Steel`,
        },
        {
          'pt-BR': `Possibilidade de oferecer prêmios às equipes participantes`,
          'en-US': `Possibility to offer awards to the competing teams`,
        },
        {
          'pt-BR': `Possibilidade de apresentação de até 5 minutos do patrocinador
          na abertura do evento`,
          'en-US': `Possibility to make a up to a 5 minutes presentation about the
          sponsor in the opening of the event`,
        },
      ],
    },
    {
      name: 'Silver',
      prices: {
        'pt-BR': 15000,
        'en-US': 4500,
      },
      benefitsList: [
        {
          'pt-BR': `Todos os benefícios da Bronze`,
          'en-US': `All benefits from Bronze`,
        },
        {
          'pt-BR': `Possibilidade de ter um representante do patrocinador entre
          os mentores do evento`,
          'en-US': `Possibility to have a sponsor's representative between the mentors
          of the event`,
        },
        {
          'pt-BR': `Possibilidade de apresentação de até 15 minutos do patrocinador
          na abertura do evento`,
          'en-US': `Possibility to make a up to 15 minutes presentation about the
          sponsor in the opening of the event`,
        },
      ],
    },
    {
      name: 'Gold',
      prices: {
        'pt-BR': 20000,
        'en-US': 6000,
      },
      benefitsList: [
        {
          'pt-BR': `Todos os benefícios da Silver`,
          'en-US': `All benefits from Silver`,
        },
        {
          'pt-BR': `Possibilidade de acesso aos emails dos participantes do evento
          que consentiram tal acesso`,
          'en-US': `Possibility to access the emails of the participants of the event
          that allowed it`,
        },
        {
          'pt-BR': `Possibilidade de apontar um representante do patrocinador
          como juíz do evento`,
          'en-US': `Possibility to point a sponsor's representative as a judge of
          the event`,
        },
      ],
    },
  ],
};

export default deephack;
