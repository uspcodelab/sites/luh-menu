import { Event, Messages } from '../types';

export const interHackMessages: Messages = {
  'pt-BR': {
    title: 'Campeonato InterHack',
    date: '24-25 de Agosto e 9-10 de Novembro de 2019',
    local: 'IME - USP, ICMC - USP, EACH - USP',
    description: `O Campeonato InterHack é o evento principal de 2019. Consiste em 4 hackathons:
    3 eliminatórias regionais em agosto e a grande final, em novembro. As elimininatórias ocorrerão
    simultâneamente nos campi Butantã (IME-USP), São Carlos (ICMC-USP) e Leste (EACH).
    De cada eliminatória, serão selecionadas 5 equipes para competir na final em novembro. A final irá ocorrer
    no campus Butantã, podendo valer uma viagem com duração de 1 semana à um grande polo tecnológico
    nacional.`,
  },
  'en-US': {
    title: 'InterHack Championship',
    date: 'August 24-25 and November 9-10 2019',
    local: 'IME - USP, ICMC - USP, EACH - USP',
    description: `The InterHack Championship is the main event of 2019. It will consist of 4 hackathons:
    3 regional selective in August and the big final, in November. The first round of hackathons will happen
    simultaneously at the Campi Butantã (IME-USP), São Carlos (ICMC-USP) e Leste (EACH).
    In the end, 5 teams will be selected to compete in the final event in November. The final will be hosted
    at the campus Butantã, with the possibility for the winning team for a one week trip to
    a great national tech center. `,
  },
};

const interhack: Event = {
  id: 'interhack',
  participants: 180,
  color: 'red',
  linearGradient: 'linear-gradient(180deg, #FF0000 0%, #FF9090 100%)',
  quotas: [
    {
      name: 'Copper',
      prices: {
        'pt-BR': 5000,
        'en-US': 1500,
      },
      benefitsList: [
        {
          'pt-BR': `Agradecimento e reconhecimento do patrocinador em todas as
          atividades relacionadas ao evento.`,
          'en-US': `Thanks and recognition to the sponsor in all activities releated
          to the event`,
        },
        {
          'pt-BR': `Possibilidade de distribuir folders, brindes e outros materiais
          relevantes do patrocinador`,
          'en-US': `Possibility to distribute folders, gifts and other
          relevant materials from the sponsor`,
        },
        {
          'pt-BR': `Identificação da marca patrocinadora em todo material de
          divulgação do evento: online e offline`,
          'en-US': `Identification of the sposoring brand in all marketing material
          online and offline`,
        },
        {
          'pt-BR': `Acesso à final com todos os benefícios`,
          'en-US': `Access at the final with all benefits`,
        },
      ],
    },
    {
      name: 'Steel',
      prices: {
        'pt-BR': 10000,
        'en-US': 3000,
      },
      benefitsList: [
        {
          'pt-BR': `Todos os benefícios da Copper`,
          'en-US': `All benefits from Copper`,
        },
        {
          'pt-BR': `Oportunidade de trazer visitantes ao evento`,
          'en-US': `Opportunity to bring visitors to the event`,
        },
        {
          'pt-BR': `Oportunidade de exibir banner do patrocinador no evento`,
          'en-US': `Opportunity to bring a sponsor's banner to the event`,
        },
      ],
    },
    {
      name: 'Bronze',
      prices: {
        'pt-BR': 15000,
        'en-US': 4500,
      },
      benefitsList: [
        {
          'pt-BR': `Todos os benefícios da Steel`,
          'en-US': `All benefits from Steel`,
        },
        {
          'pt-BR': `Possibilidade de oferecer prêmios às equipes participantes`,
          'en-US': `Possibility to offer awards to the competing teams`,
        },
        {
          'pt-BR': `Possibilidade de apresentação de até 5 minutos do patrocinador
          na abertura do evento`,
          'en-US': `Possibility to make a up to a 5 minutes presentation about the
          sponsor in the opening of the event`,
        },
      ],
    },
    {
      name: 'Silver',
      prices: {
        'pt-BR': 20000,
        'en-US': 6000,
      },
      benefitsList: [
        {
          'pt-BR': `Todos os benefícios da Bronze`,
          'en-US': `All benefits from Bronze`,
        },
        {
          'pt-BR': `Possibilidade de ter um representante do patrocinador entre
          os mentores do evento`,
          'en-US': `Possibility to have a sponsor's representative between the mentors
          of the event`,
        },
        {
          'pt-BR': `Possibilidade de apresentação de até 15 minutos do patrocinador
          na abertura do evento`,
          'en-US': `Possibility to make a up to 15 minutes presentation about the
          sponsor in the opening of the event`,
        },
      ],
    },
    {
      name: 'Gold',
      prices: {
        'pt-BR': 30000,
        'en-US': 7500,
      },
      benefitsList: [
        {
          'pt-BR': `Todos os benefícios da Silver`,
          'en-US': `All benefits from Silver`,
        },
        {
          'pt-BR': `Possibilidade de acesso aos emails dos participantes do evento
          que consentiram tal acesso`,
          'en-US': `Possibility to access the emails of the participants of the event
          that allowed it`,
        },
        {
          'pt-BR': `Possibilidade de apontar um representante do patrocinador
          como juíz do evento`,
          'en-US': `Possibility to point a sponsor's representative as a judge of
          the event`,
        },
      ],
    },
  ],
};

export default interhack;
