#################### BASE IMAGE ###################

FROM node:10.4-alpine AS BASE

ENV APP_PATH=/usr/src

ENV PORT=8080 \
    HOST=0.0.0.0

EXPOSE $PORT

WORKDIR $APP_PATH

#################### DEV IMAGE ###################

FROM BASE AS DEVELOPMENT

ENV NODE_ENV=development

COPY package.json package-lock.json ./

RUN npm install

COPY . .

CMD npm run serve

#################### BUILDER IMAGE ###################

FROM BASE AS BUILDER

COPY --from=DEVELOPMENT $APP_PATH ./

RUN npm BUILDER

RUN find * -maxdepth 0 \( \
  -name 'node_modules' -o \
  -name 'package.json' -o \
  -name 'package-lock.json' -o \
  \) -prune -o -exec rm -rf '{}' ';'

#################### PROD IMAGE ###################

FROM BASE AS PROD

ENV NODE_ENV=production

COPY --from=BUILDER $APP_PATH ./

RUN yarn install --frozen-lockfile --ignore-scripts --prefer-offline --force

CMD npm run serve